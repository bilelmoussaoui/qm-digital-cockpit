%define _version 0.0.1
%define _release 1
%define _user digital-cockpit
%define _group digital-cockpit
%define _uid 1020
%define _gid 1020

%global debug_package %{nil}

Name:           qm-digital-cockpit
Version:        %{_version}
Release:        %{_release}%{?dist}
Summary:        qm-digital-cockpit files and services

License:        MIT
URL:            http://fedoraproject.org/
Source:         %{name}-%{version}.tar.xz

BuildRequires:  meson
BuildRequires:  systemd
BuildRequires:  systemd-rpm-macros
BuildArch:      noarch
Requires(pre):  /usr/sbin/useradd, /usr/bin/getent, /usr/bin/mkdir, /usr/bin/chown
Requires:       jq

%description
This package contains the qm-digital-cockpit assets


%package selinux-policy
Summary:        SELinux policy module for %{name}
BuildRequires:  selinux-policy
BuildRequires:  selinux-policy-devel
BuildRequires:  make
BuildArch:      noarch
%{?selinux_requires}

%description selinux-policy
This package contains the SELinux policy module for %{name}.

%package firefox
Summary:        Firefox container for %{name}
BuildArch:      noarch

%description firefox
This package contains the Firefox container for %{name}.

%package simple-shell
Summary:        Simple-shell container for %{name}
BuildArch:      noarch

%description simple-shell
This package contains the Simple-shell container for %{name}.

%package weston
Summary:        Weston compositor container for %{name}
BuildArch:      noarch

%description weston
This package contains the Weston compositor container for %{name}.

%prep
%autosetup -p1

%build
%meson                 \
  -Ddc_user=%{_user}   \
  -Ddc_group=%{_group} \
  -Ddc_uid=%{_uid}     \
  -Ddc_uid=%{_gid}     \
  %{nil}
%meson_build

%install
%meson_install

%pre
if [ $1 = 1 ]; then
  # Initial installation
  /usr/bin/getent group %{_group} >/dev/null || /usr/sbin/groupadd -g %{_gid} %{_group}
  if ! /usr/bin/getent passwd %{_user} >/dev/null ; then
      /usr/sbin/useradd -u %{_uid} -g %{_group} -m -d %{_sharedstatedir}/%{_user} -c "%{name}" %{_user}
  fi
fi

%post
%systemd_post digital-cockpit.service
%systemd_post digital-cockpit-sm.service
%systemd_user_post dbus-broker.service
%systemd_user_post dbus.socket

%preun
%systemd_preun digital-cockpit.service
%systemd_preun digital-cockpit-sm.service
%systemd_user_preun dbus-broker.service
%systemd_user_preun dbus.socket

%post selinux-policy
%selinux_modules_install %{_datadir}/selinux/packages/qm-digital-cockpit.pp.bz2

%postun selinux-policy
if [ $1 -eq 0 ]; then
    %selinux_modules_uninstall %{_datadir}/selinux/packages/qm-digital-cockpit.pp.bz2
fi

%post weston
%systemd_post weston.socket

%preun weston
%systemd_preun weston.socket

%post simple-shell
%systemd_post simple-shell.socket

%preun simple-shell
%systemd_preun simple-shell.socket

%files
%license LICENSE
%attr(0755,root,root) %{_prefix}/bin/digital-cockpit-sm
%attr(0644,root,root) %{_prefix}/lib/tmpfiles.d/digital-cockpit.conf
%attr(0644,root,root) %{_prefix}/lib/systemd/system/digital-cockpit.service
%attr(0644,root,root) %{_prefix}/lib/systemd/system/digital-cockpit-sm.service
%attr(0644,root,root) %{_prefix}/lib/systemd/user/digital-cockpit-session.target
%attr(0644,root,root) %{_prefix}/lib/systemd/system-preset/*-digital-cockpit.preset
%attr(0644,root,root) %{_prefix}/lib/systemd/user-preset/*-digital-cockpit.preset

%files selinux-policy
%defattr(-,root,root,0755)
%attr(0644,root,root) %{_datadir}/selinux/packages/qm-digital-cockpit.pp.bz2
%attr(0644,root,root) %{_datadir}/selinux/devel/include/contrib/qm-digital-cockpit.if

%files firefox
%defattr(-,root,root,0755)
%attr(0644,root,root) %{_sysconfdir}/containers/systemd/firefox.container

%files simple-shell
%defattr(-,root,root,0755)
%attr(0644,root,root) %{_prefix}/lib/systemd/system/simple-shell.socket
%attr(0644,root,root) %{_sysconfdir}/containers/systemd/simple-shell.container
%attr(0644,root,root) %{_prefix}/lib/systemd/system-preset/*-simple-shell.preset

%files weston
%defattr(-,root,root,0755)
%attr(0644,root,root) %{_prefix}/lib/systemd/system/weston.socket
%attr(0644,root,root) %{_sysconfdir}/containers/systemd/weston.container
%attr(0644,root,root) %{_prefix}/lib/systemd/system-preset/*-weston.preset

%changelog
* Fri Nov 10 2023 Roberto Majadas <rmajadas@redhat.com>
-